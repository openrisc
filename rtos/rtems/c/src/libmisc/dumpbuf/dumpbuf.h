/*
 *  COPYRIGHT (c) 1997.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may in
 *  the file LICENSE in this distribution or at
 *  http://www.OARcorp.com/rtems/license.html.
 *
 *  $Id: dumpbuf.h,v 1.2 2001-09-27 12:01:42 chris Exp $
 */

#ifndef __DUMP_BUFFER_h
#define __DUMP_BUFFER_h

void Dump_Buffer(
  unsigned char *buffer,
  int            length
);

#endif
/* end of include file */
