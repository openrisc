/*
 * $Id: clock.h,v 1.2 2001-09-27 12:01:19 chris Exp $
 */

extern void a29k_init_timer( unsigned32 timer_clock_interval );
extern void a29k_disable_timer(void);
extern void a29k_clear_timer(void);
