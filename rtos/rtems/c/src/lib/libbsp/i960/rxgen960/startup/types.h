/*
 *  $Id: types.h,v 1.2 2001-09-27 11:59:59 chris Exp $
 */

/* Type definitions for our use */
#ifndef __TYPES_H
#define __TYPES_H
typedef unsigned char u8;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char uchar;
typedef unsigned int Int32;
typedef unsigned short Int16;
typedef unsigned char Int8;
typedef unsigned int u_int;
typedef unsigned int u_long;
typedef unsigned short u_short;
typedef unsigned char u_char;
#ifndef NULL
#define NULL 0
#endif
#ifndef TRUE 
#define TRUE 1
#endif
#ifndef FALSE 
#define FALSE 0
#endif
/*
 * Definitions of unsigned amounts
 */
#define ub      Int8
#define uw      Int16
#define ul      Int64
#define sl      long
#define ui      Int32


#endif
