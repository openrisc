/*-------------------------------------*/
/* asmfault.h                          */
/* Last change :  3.11.94              */
/*-------------------------------------*/
/*
 *  $Id: asmfault.h,v 1.2 2001-09-27 11:59:59 chris Exp $
 */

#ifndef _ASMFAULT_H_
#define _ASMFAULT_H_

  /* Fault handler start point. 
   */
extern void faultHndlEntry(void);

#endif
/*-------------*/
/* End of file */
/*-------------*/

