#
#  $Id: README,v 1.2 2001-09-27 12:01:11 chris Exp $
#
#  Author: Ralf Corsepius (corsepiu@faw.uni-ulm.de)
#  Adapted by: John Mills (jmills@tga.com)
#

BSP NAME:           generic SH2 (gensh2)
BOARD:              n/a
BUS:                n/a
CPU FAMILY:         Hitachi SH
CPU:                SH 7045F
COPROCESSORS:	    none
MODE:               n/a

DEBUG MONITOR:      gdb

PERIPHERALS
===========
TIMERS:             on-chip
  RESOLUTION:         cf. Hitachi SH 704X Hardware Manual (Phi/4)
SERIAL PORTS:       on-chip (with 2 ports)
REAL-TIME CLOCK:    none
DMA:                not used
VIDEO:              none
SCSI:               none
NETWORKING:         none

DRIVER INFORMATION
==================
CLOCK DRIVER:       on-chip timer
IOSUPP DRIVER:      default
SHMSUPP:            default
TIMER DRIVER:       on-chip timer
TTY DRIVER:         /dev/null (stub)

STDIO
=====
PORT:               /dev/null (stub)
ELECTRICAL:         n/a
BAUD:               n/a
BITS PER CHARACTER: n/a
PARITY:             n/a
STOP BITS:          n/a

NOTES
=====

(1) Only stub console driver available at the moment.
    Driver for the on-chip serial devices (sci) will be available soon.

(2) The present 'hw_init.c' file provides 'early_hw_init'(void) which
    is normally called from 'start.S' to provide such minimal HW setup
    as is conveniently written in 'C' and can make use of global
    symbols for 7045F processor elements. It also provides
    'void bsp_hw_init (void)' normally called from 'bspstart.c', shortly
    before RTEMS itself is started.

    These are both minimal functions intended to support the RTEMS test
    suites.
