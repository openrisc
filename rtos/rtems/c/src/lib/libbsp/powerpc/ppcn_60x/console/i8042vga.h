/*
 *  COPYRIGHT (c) 1998 by Radstone Technology
 *
 *
 * THIS FILE IS PROVIDED TO YOU, THE USER, "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK
 * AS TO THE QUALITY AND PERFORMANCE OF ALL CODE IN THIS FILE IS WITH YOU.
 *
 * You are hereby granted permission to use, copy, modify, and distribute
 * this file, provided that this notice, plus the above copyright notice
 * and disclaimer, appears in all copies. Radstone Technology will provide
 * no support for this code.
 *
 *  $Id: i8042vga.h,v 1.2 2001-09-27 12:00:49 chris Exp $
 */

#ifndef _I8042VGA_H_
#define _I8042VGA_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Driver function table
 */
extern console_fns i8042vga_fns;

#ifdef __cplusplus
}
#endif

#endif /* _I8042VGA_H_ */
