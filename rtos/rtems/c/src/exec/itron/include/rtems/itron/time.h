/*
 *  COPYRIGHT (c) 1989-1999.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.OARcorp.com/rtems/license.html.
 *
 *  $Id: time.h,v 1.2 2001-09-27 11:59:12 chris Exp $
 */

#ifndef __RTEMS_ITRON_TIME_h_
#define __RTEMS_ITRON_TIME_h_

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  XXX insert private stuff here
 */

#include <rtems/itron/time.inl>

#ifdef __cplusplus
}
#endif

#endif
/* end of include file */

