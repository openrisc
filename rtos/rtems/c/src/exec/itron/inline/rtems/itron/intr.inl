/*
 *  COPYRIGHT (c) 1989-1999.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.OARcorp.com/rtems/license.html.
 *
 *  $Id: intr.inl,v 1.2 2001-09-27 11:59:13 chris Exp $
 */

#ifndef __ITRON_INTERRUPT_inl_
#define __ITRON_INTERRUPT_inl_

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  XXX insert inline routines here
 */


#ifdef __cplusplus
}
#endif

#endif
/* end of include file */

