/*
 *  Interrupt Manager
 *
 *
 *  COPYRIGHT (c) 1989-1999.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.OARcorp.com/rtems/license.html.
 *
 *  $Id: intr.c,v 1.2 2001-09-27 11:59:19 chris Exp $
 */

#include <rtems/system.h>
#include <rtems/rtems/status.h>
#include <rtems/score/isr.h>
#include <rtems/rtems/intr.h>

/*  _Interrupt_Manager_initialization
 *
 *  This routine initializes the interrupt manager.
 *
 *  Input parameters: NONE
 *
 *  Output parameters: NONE
 */

void _Interrupt_Manager_initialization( void )
{
}
