/*  inline/event.inl
 *
 *  This include file contains the static inline implementation of
 *  macros for the Event Manager.
 *
 *  COPYRIGHT (c) 1989-1999.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.OARcorp.com/rtems/license.html.
 *
 *  $Id: event.inl,v 1.2 2001-09-27 11:59:18 chris Exp $
 */

#ifndef __MACROS_EVENT_inl
#define __MACROS_EVENT_inl

#endif
/* end of include file */
