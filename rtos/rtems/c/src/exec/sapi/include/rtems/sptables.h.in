/*  sptables.h
 *
 *  This include file contains the executive's pre-initialized tables
 *  used when in a single processor configuration.
 *
 *  COPYRIGHT (c) 1989-1999.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.OARcorp.com/rtems/license.html.
 *
 *  $Id: sptables.h.in,v 1.2 2001-09-27 11:59:19 chris Exp $
 */

#ifndef __RTEMS_SPTABLES_h
#define __RTEMS_SPTABLES_h

#ifdef __cplusplus
extern "C" {
#endif

#include <rtems/config.h>

#include <rtems/debug.h>
#include <rtems/fatal.h>
#include <rtems/init.h>
#include <rtems/io.h>
#include <rtems/score/sysstate.h>

#include <rtems/rtems/intr.h>
#include <rtems/rtems/clock.h>
#include <rtems/rtems/tasks.h>
#include <rtems/rtems/dpmem.h>
#include <rtems/rtems/event.h>
#include <rtems/rtems/message.h>
#if defined(RTEMS_MULTIPROCESSING)
#include <rtems/rtems/mp.h>
#endif
#include <rtems/rtems/part.h>
#include <rtems/rtems/ratemon.h>
#include <rtems/rtems/region.h>
#include <rtems/rtems/sem.h>
#include <rtems/rtems/signal.h>
#include <rtems/rtems/timer.h>

/*
 *  This is the default Multiprocessing Configuration Table.
 *  It is used in single processor configurations.
 */

#if defined(SAPI_INIT) 
const rtems_multiprocessing_table
       _Initialization_Default_multiprocessing_table = {
  1,                      /* local node number */
  1,                      /* maximum number nodes in system */
  0,                      /* maximum number global objects */
  0,                      /* maximum number proxies */
  NULL,                   /* pointer to MPCI address table */
};
#else
extern const rtems_multiprocessing_table
       _Initialization_Default_multiprocessing_table;
#endif

/*
 *  This is the version string.
 */

#define RTEMS_VERSION "rtems-@RTEMS_VERSION@"

#if defined(SAPI_INIT) 
const char _RTEMS_version[] =
  "RTEMS RELEASE " RTEMS_VERSION
     "(" CPU_NAME "/" CPU_MODEL_NAME "/@RTEMS_BSP@)";
#else
extern const char _RTEMS_version[];
#endif

#ifdef __cplusplus
}
#endif

#endif
/* end of include file */
