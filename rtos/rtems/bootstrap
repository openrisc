#!/bin/sh
#
# helps bootstrapping, when checked out from CVS
# requires GNU autoconf and GNU automake
#
# $Id: bootstrap,v 1.2 2001-09-27 11:59:04 chris Exp $

# this is not meant to be exported outside the source tree

# NOTE: Inspired by libtool's autogen script

# to be run from the toplevel directory of RTEMS'
# source tree

progname=`basename $0`
top_srcdir=`dirname $0`

verbose="";
quiet="false"
mode="generate"

usage()
{
  echo
  echo "usage: ${progname} [-h|-q|-v]"
  echo
  echo "options:"
  echo "	-h .. display this message and exit";
  echo "	-q .. quiet, don't display directories";
  echo "	-v .. verbose, pass -v to automake when invoking automake"
  echo "	-c .. clean, remove all aclocal/autoconf/automake generated files"
  echo
  exit 1;
}

if test ! -f $top_srcdir/VERSION; then
  echo "${progname}:"
  echo "	Installation problem: Can't find file VERSION"
  exit 1;
fi

while test $# -gt 0; do
case $1 in
-h|--he|--hel|--help)
  usage ;;
-q|--qu|--qui|--quie|--quiet)
  quiet="true";
  shift;;
-v|--ve|--ver|--verb|--verbo|--verbos|--verbose)
  verbose="-v";
  shift;;
-c|--cl|--cle|--clea|--clean)
  mode="clean";
  shift;;
-*) echo "unknown option $1" ;
  usage ;;
*) echo "invalid parameter $1" ;
  usage ;;
esac
done

case $mode in
generate)

  case $top_srcdir in
  /* ) aclocal_dir=$top_srcdir/aclocal
    ;;
  *) aclocal_dir=`pwd`/$top_srcdir/aclocal
    ;;
  esac

  confs=`find . -name 'configure.in' -print`
  for i in $confs; do
  dir=`dirname $i`;
  ( test "$quiet" = "true" || echo "$dir";
    cd $dir;
    aclocal -I $aclocal_dir;
    autoconf;
    test -n "`grep CONFIG_HEADER configure.in`" && autoheader ;
    test -f Makefile.am && automake $verbose ;
    test -f Makefile.am && test -n "`grep 'stamp-h\.in' Makefile.in`" \
      && echo timestamp > stamp-h.in
  )
  done
  ;;

clean)
  test "$quiet" = "true" || echo "removing automake generated Makefile.in files"
  files=`find . -name 'Makefile.am' -print | sed -e 's%\.am%\.in%g'` ;
  for i in $files; do if test -f $i; then
    rm -f $i
    test "$verbose" = "-v" && echo "$i"    
  fi; done

  test "$quiet" = "true" || echo "removing configure files"
  files=`find . -name 'configure' -print` ;
  test "$verbose" = "-v" && test -n "$files" && echo "$files" ;
  for i in $files; do if test -f $i; then
    rm -f $i
    test "$verbose" = "-v" && echo "$i"    
  fi; done
  
  test "$quiet" = "true" || echo "removing aclocal.m4 files"
  files=`find . -name 'aclocal.m4' -print` ;
  test "$verbose" = "-v" && test -n "$files" && echo "$files" ;
  for i in $files; do if test -f $i; then
    rm -f $i
    test "$verbose" = "-v" && echo "$i"    
  fi; done

  find . -name '*~' -print | xargs rm -f
  find . -name '*.orig' -print | xargs rm -f
  find . -name '*.rej' -print | xargs rm -f
  find . -name 'config.status' -print | xargs rm -f
  find . -name 'config.log' -print | xargs rm -f
  find . -name 'config.cache' -print | xargs rm -f
  find . -name 'Makefile' -print | xargs rm -f
  find . -name '.deps' -print | xargs rm -rf
  find . -name '.libs' -print | xargs rm -rf
  find . -name 'stamp-h.in' | xargs rm -rf
  ;;
esac
