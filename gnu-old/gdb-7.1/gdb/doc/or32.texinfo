\input texinfo      @c -*-texinfo-*-
@c Copyright (C) 2008 Embecosm Limited
@c
@c %**start of header
@c makeinfo ignores cmds prev to setfilename, so its arg cannot make use
@c of @set vars.  However, you can override filename with makeinfo -o.
@setfilename or32.info
@c
@include gdb-cfg.texi
@c
@settitle Debugging the OpenRISC 1000 with @value{GDBN}
@setchapternewpage odd
@c %**end of header

@iftex
@c @smallbook
@c @cropmarks
@end iftex

@finalout
@syncodeindex ky cp

@c readline appendices use @vindex, @findex and @ftable,
@c annotate.texi and gdbmi use @findex.
@syncodeindex vr cp
@syncodeindex fn cp

@c !!set manual's edition!
@set EDITION Second

@c !!set GDB edit command default editor
@set EDITOR /bin/ex

@c THIS MANUAL REQUIRES TEXINFO 4.0 OR LATER.

@c This is a dir.info fragment to support semi-automated addition of
@c manuals to an info tree.
@dircategory Software development
@direntry
* Gdb for OpenRISC 1000: (gdb for Or32).   The GNU debugger for OpenRISC 1000.
@end direntry

@ifinfo
This file documents the @sc{gnu} debugger @value{GDBN} when used with
OpenRISC 1000 processors.

This is the @value{EDITION} Edition, of @cite{Debugging the OpenRISC 1000
@value{GDBN}} for @value{GDBN}
Version @value{GDBVN}.

Copyright (C) 2008 Embecosm Limited

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 3 or
any later version published by the Free Software Foundation; with the
Front-Cover Texts being ``Debugging the OpenRISC 1000 with GDB by
Jeremy Bennett'' and with the Back-Cover Texts being ``You are free to
copy and modify this Manual.''
@end ifinfo

@titlepage
@title Debugging the OpenRISC 1000 with @value{GDBN}
@subtitle Target Processor Manual
@sp 1
@subtitle @value{EDITION} Edition, for @value{GDBN} version @value{GDBVN}
@author Jeremy Bennett, Embecosm Limited
@page
@tex
{\parskip=0pt
\hfill Please report bugs using the OpenRISC bug tracker:\par
\hfill @uref{http://opencores.org/openrisc,bugtracker}.\par
\hfill {\it Debugging the OpenRISC 1000 with @value{GDBN}}\par
\hfill \TeX{}info \texinfoversion\par
}
@end tex

@vskip 0pt plus 1filll
Copyright @copyright{} 2008
Embecosm Limited.
@sp 2
Published by Embecosm Limited@*
Palamos House #104
Lymington SO41 9AL, UK@*

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 3 or
any later version published by the Free Software Foundation; with the
Front-Cover Texts being ``Debugging the OpenRISC 1000 with GDB by
Jeremy Bennett'' and with the Back-Cover Texts being ``You are free to
copy and modify this Manual.''
@end titlepage
@page

@ifnottex
@node Top, Summary, (dir), (dir)

@top Debugging the OpenRISC 1000 with @value{GDBN}

This file describes @value{GDBN}, the @sc{gnu} symbolic debugger for
use with the OpenRISC 1000 processor architecture.

This is the @value{EDITION} Edition, for @value{GDBN} Version
@value{GDBVN}.

Copyright (C) 2008 Embecosm Limited

@menu
* Summary::                         Summary of @value{GDBN} with OpenRISC 1000
* Connecting to the Target::        Connecting to an OpenRISC 1000 Target
* OpenRISC 1000 Specific Commands:: Commands just for the OpenRISC 1000
* OpenRISC 1000 Example::           A small example
* OpenRISC 1000 Limitations::       Known problems

* Copying::			    GNU General Public License says
                                    how you can copy and share GDB
* GNU Free Documentation License::  The license for this documentation
* Index::                           Index
@end menu

@end ifnottex

@contents

@node Summary
@unnumbered Summary of @value{GDBN} for the OpenRISC 1000
@cindex Overview
@cindex Summary

@value{GDBN} is described well in its user manual, ``Debugging with GDB: The
GNU Source-Level Debugger''.

@cindex RSP
@cindex Remote Serial Protocol
This manual describes how to use @value{GDBN} to debug C programs cross
compiled for and running on processors using the OpenRISC 1000
architecture. In general @value{GDBN} does not run on the actual target, but
on a separate host processor. It communicates with the target via the
@value{GDBN} @dfn{Remote Serial Protocol} (@acronym{RSP}).

@cindex JTAG
@cindex jtag, target
@cindex target jtag
Past releases of @value{GDBN} for OpenRISC supported a custom remote
protocol, which drives the JTAG interface on the OpenRISC 1000. This is
now obsolete, since all targets support the @dfn{Remote Serial
Protocol}, with adapters available to drive JTAG. Support has been
dropped from @value{GDBN} release 7.1.

@cindex simulator,
@cindex simlator, target
@cindex target simulator
This release implements a simulator, based on Or1ksim, the OpenRISC
architectural simulator. @command{target sim} will connect to a vanilla
Or1ksim model with 8MB of RAM starting at address zero.

@cindex SPR
@cindex Special Purpose Registers
@cindex @command{spr} command
@cindex commands, @command{spr}
@cindex @command{info spr} command
@cindex commands, @command{info spr}
@cindex OpenRISC 1000 specific commands
@cindex commands, OpenRISC 1000 specific
In addition the info command is extended to allow inspection of
OpenRISC 1000 Special Purpose registers, and a new command ``spr'' is
added to set the value of a Special Purpose Register. @xref{OpenRISC 1000
Specific Commands,,OpenRISC 1000 Specific Commands}.

@cindex @command{info registers} command  for OpenRISC 1000
@cindex commands, @command{info registers} for OpenRISC 1000
All the normal GDB commands should work, although hardware watchpoints are not
tested at present. The @command{info registers} command will show the 32 general
purpose registers, while the @command{info registers all} command will add the
program counter, supervision register and exception program counter register.

@iftex
Throughout this document, user input is emphasised like this: @command{input},
program output is show like this: @code{Hello World!}.
@end iftex

@cindex graphical debugging
@cindex graphical debugging, @command{gdbtui}
@cindex graphical debugging, @command{ddd}
@cindex @command{gdbtui}
@cindex @command{ddd}
For those who like their debugging graphical, the @command{gdbtui} command is
available (typically as @command{or32-elf-gdbtui}). @value{GDBN} for
OpenRISC 1000 can also be run under @command{ddd} as follows:

@example
@command{ddd --debugger=or32-elf-gdb --gdb}
@end example

@menu
* Contributors::                Contributors to GDB for the OpenRISC 1000
@end menu

@node Contributors
@unnumberedsec Contributors to @value{GDBN} for the OpenRISC 1000

The pantheon of contributors to GDB over the years is recorded in the main
user manual, `Debugging with GDB: The GNU Source-Level Debugger''.

@cindex contributors, OpenRISC 1000
There is no official history of contributors to the OpenRISC 1000
version. However the current author believes the original @value{GDBN}
5.0 and 5.3 ports were the work of:

@itemize @bullet
@item
@cindex Ivan Guzvinex
@cindex Guzvinex, Ivan
@cindex Johan Rydverg
@cindex Rydverg, Johan
@cindex Binary File Description library
@cindex BFD
Ivan Guzvinec and Johan Rydverg at OpenCores, who wrote the Binary File
Descriptor library;

@item
@cindex Alessandro Forin
@cindex Forin, Alessandro
@cindex Per Bothner
@cindex Bothner, Per
@cindex GDB interface, OpenRISC 1000
Alessandro Forin at Carnegie-Mellon University and Per Bothner at the University
of Wisconsin who wrote the main GDB interface; and

@item
@cindex Mark Mlinar
@cindex Mlinar, Mark
@cindex Chris Ziomkowski
@cindex Ziomkowski, Chris
@cindex OpenRISC 1000 JTAG interface
@cindex JTAG, OpenRISC 1000 interface
Mark Mlinar at Cygnus Support and Chris Ziomkowski at ASICS.ws, who wrote the
OpenRISC JTAG interface (now obsolete).
@end itemize

@cindex Jeremy Bennett
@cindex Bennett, Jeremy
@cindex Embecosm
The port to @value{GDBN} @value{GDBVN} is the work of Jeremy Bennett
of Embecosm Limited (jeremy.bennett@@embecosm.com).

@quotation Plea
@cindex contributors, unknown
If you know of anyone who has been omitted from this list, please email the
current author, so the omission can be corrected, and credit given where it is
due.
@end quotation

@node Connecting to the Target
@chapter Connecting to an OpenRISC 1000 Target
@cindex OpenRISC 1000 target, connecting
@cindex target, OpenRISC 1000, connecting
@cindex connecting, OpenRISC 1000 target
There are two ways to connect to an OpenRISC 1000 target with GDB.

@enumerate
@item
@cindex @command{target remote} command
@cindex commands, @command{target remote}
@cindex @command{target extended-remote} command
@cindex commands, @command{target extended-remote}
@cindex OpenRISC 1000 target, remote connecting via RSP
@cindex target, remote, OpenRISC 1000, connecting via RSP
@cindex connecting, OpenRISC 1000 target, remote via RSP
@cindex remote OpenRISC 1000 target, connecting via RSP
Via a TCP/IP socket to a machine which has the hardware connected, or
is running the architectural simulator using the standard @value{GDBN}
@dfn{Remote Serial Protocol}. This uses the @value{GDBN} commands
@command{target remote} or @command{target extended-remote}.

@item
@cindex @command{target sim} command
@cindex commands, @command{target sim}
@cindex OpenRISC 1000 target, simulator
@cindex sim, target
@cindex target sim
@cindex target, remote, OpenRISC 1000, connecting via JTAG
@cindex connecting, OpenRISC 1000 target, remote via JTAG
@cindex remote OpenRISC 1000 target, connecting via JTAG
To the OpenRISC architectural simulator, Or1ksim, integrated as a GDB
simulator.  This uses the @value{GDBN} command @command{target sim}.

@end enumerate

@quotation Note
Connection via the obsolete proprietary OpenRISC JTAG protocol is no
longer supported.
@end quotation

@cindex OpenRISC 1000 Architectural Simulator
@cindex Or1ksim
@quotation Caution
This release of GDB requires the latest experimental version of Or1ksim,
built from SVN revision 229 or later.
@end quotation

@menu
* Remote Serial Protocol Connection:: Connection via the @value{GDBN} Remote
                                      Serial Protocol Interface
* Simulator Connection::              Connection to the built in simulator
@end menu

@node Remote Serial Protocol Connection
@section Connection via the @value{GDBN} Remote Serial Protocol
@cindex OpenRISC 1000 target, remote connecting via RSP
@cindex target, remote, OpenRISC 1000, connecting via RSP
@cindex connecting, OpenRISC 1000 target, remote via RSP
@cindex remote OpenRISC 1000 target, connecting via RSP

The usual mode of operation is through the @value{GDBN} @dfn{Remote Serial
Protocol} (@acronym{RSP}). This communicates to the target through a TCP/IP
socket. The target must then implement the server side of the interface to
drive either physical hardware (for example through a USB/JTAG connector) or a
simulation of the hardware (such as the OpenRISC Architectural Simulator).

Although referred to as a @emph{remote} interface, the target may actually
be on the same machine, just running in a separate process, with its own
terminal window.

For example, to connect to the OpenRISC 1000 Architectural simulator, which is
running on machine ``thomas'' and has been configured to talk to @value{GDBN}
on port 51000, the following command would be used:

@cindex remote @command{target jtag} command
@cindex @command{target jtag} command, remote
@cindex commands, @command{target jtag}, remote
@cindex remote target specification for RSP
@cindex target specification for RSP
@example
@command{target remote thomas:51000}
@end example

The target machine is specified as the machine name and port number. If the
architectural simulator was running on the same machine, its name may be
omitted, thus:

@cindex remote target specification, same machine for RSP
@cindex target specification for RSP, same machine
@example
@command{target remote :51000}
@end example

@node Simulator Connection
@section Connection to the Built in Simulator
@cindex OpenRISC 1000 target, simulator
@cindex target, simulator, OpenRISC 1000, connecting
@cindex connecting, OpenRISC 1000 target, simulator
@cindex simulator OpenRISC 1000 target, connecting

The simplest way to run programs under @value{GDBN} is to connect to the
built in simulator. This is the OpenRISC architectural simulator,
Or1ksim, which has been integrated into GDB as a standard simulator.

@cindex @command{target sim} command
@cindex commands, @command{target sim}
@example
@command{target sim}
@end example

By default, the simulator is configured with 8MB of RAM running from
address 0x0, and the simulator runs with the Or1ksim @command{--quiet}
option, to mimize extraneous output.

@cindex @command{target sim}, additional options
Additional options may be specified to the underlying Or1ksim engine,
exactly as when using Or1ksim standalone, with the entire argument
string prefixed by @command{-f}.  For example.

@example
@command{target sim "-f --report-memory-errors -f mysim.cfg"}
@end example

@quotation Note
It is possible to use @command{target sim} many times. However any arguments
are only applied the first time. Or1ksim can only be instantiated
once. On the completion of a run it is not actually cosed, merely
stalled.
@end quotation

@quotation Caution 
Any additional configuration must take account of the existing
8MB memory block. At present there is no way to remove that memory
block.
@end quotation

@node OpenRISC 1000 Specific Commands
@chapter Commands just for the OpenRISC 1000
@cindex @command{info spr} command
@cindex @command{spr} command
@cindex commands, @command{info spr}
@cindex commands, @command{spr} command
@cindex custom commands, OpenRISC 1000
@cindex OpenRISC 1000, custom commands
The OpenRISC 1000 has one particular feature that is difficult for
@value{GDBN}. @value{GDBN} models target processors with a register
bank and a block of memory. The internals of @value{GDBN} assume that
there are not a huge number of registers in total.

The OpenRISC 1000 Special Purpose Registers (SPR) do not really fit well into
this structure. There are too many of them (12 groups each with 2000+ entries
so far, with up to 32 groups permitted) to be implemented as ordinary
registers in @value{GDBN}. Think what this would mean for the command
@command{info registers all}. However they cannot be considered memory, since
they do not reside in the main memory map.

The solution is to add two new commands to @value{GDBN} to see the value of a
particular SPR and to set the value of a particular SPR.

@enumerate
@item
@command{info spr} is used to show the value of a SPR or group of SPRs.

@item
@command{spr} is used to set the value of an individual SPR.
@end enumerate

@menu
* Reading SPRs::            Using the ``info spr'' command
* Writing SPRs::            Using the spr command
@end menu

@node Reading SPRs
@section Using the @command{info spr} Command
@cindex @command{info spr} command
@cindex commands, @command{info spr}
@cindex @command{info spr} command, argument specification
@cindex @command{info spr} command, single register

The value of an SPR is read by specifying either the unique name of the SPR,
or the its group and index in that group. For example the Debug Reason
Register (@code{DRR}, register 21 in group 6 (Debug)) can be read using any of
the following commands:

@example
@command{info spr DRR}
@command{info spr debug DRR}
@command{info spr debug 21}
@command{info spr 6 DRR}
@command{info spr 6 21}
@end example

In each case the output will be:

@example
@code{DEBUG.DRR = SPR6_21 = 0 (0x0)}
@end example

@cindex @command{info spr} command, argument specification
@cindex @command{info spr} command, complete group
It is also possible to inspect all the registers in a group. For example to
look at all the Programmable Interrupt Controller registers (group 9), either
of the following commands could be used:

@example
@command{info spr PIC}
@command{info spr 9}
@end example

And the output would be:

@example
@code{PIC.PICMR = SPR9_0 = 0 (0x9)}
@code{PIC.PICSR = SPR9_2 = 0 (0x8)}
@end example

Indicating that interrupts 0 and 4 are enabled and interrupt 4 is pending.

@node Writing SPRs
@section Using the @command{spr} Command
@cindex @command{spr} command
@cindex commands, @command{spr} command
@cindex @command{spr} command, argument specification

The value of an SPR is written by specifying the unique name of the SPR or its
group and index in the same manner as for the @command{info spr} command. An
additional argument specifies the value to be written. So for example the
Programmable Interrupt Controller mask register could be changed to enable
interrupts 5 and 3 only by any of the following commands.

@example
@command{spr PICMR 0x24}
@command{spr PIC PICMR 0x24}
@command{spr PIC 0 0x24}
@command{spr 9 PICMR 0x24}
@command{spr 9 2 0x24}
@end example

@node OpenRISC 1000 Example
@chapter A Small Example
@cindex examples
@cindex examples, Hello World
@cindex Hello World example

A simple ``Hello World'' program (what else) is used to show the basics

@cindex examples
@cindex examples, Hello World
@cindex Hello World example
This is the cannonical small program. Here is the main program and its two
subprograms (added to demonstrate a meaningful backtrace).

@example
void level2() @{
  simexit( 0 );
@}

void level1() @{
  level2();
@}

main()
@{
  int  i;
  int  j;

  simputs( "Hello World!\n" );
  level1();
@}
@end example

It is linked with a program providing the utility functions @code{simexit},
@code{simputc} and @code{simprints}.

@example
void  simexit( int  rc )
@{
  __asm__ __volatile__ ( "\tl.nop\t%0" : : "K"( NOP_EXIT ));

@}	/* simexit() */

void  simputc( int  c )
@{
  __asm__ __volatile__ ( "\tl.nop\t%0" : : "K"( NOP_PUTC ));

@}	/* simputc() */

void  simputs( char *str )
@{
  int  i;

  for( i = 0; str[i] != '\0' ; i++ ) @{
    simputc( (int)(str[i]) );
  @}
@}	/* simputs() */
@end example

Finally, a small bootloader is needed, which will be placed at the OpenRISC
reset vector location (0x100) to set up a stack and jump to the main program.

@example
        .org    0x100           # The reset routine goes at 0x100
        .global _start
_start:
        l.addi  r1,r0,0x7f00    # Set SP to value 0x7f00
        l.addi  r2,r1,0x0       # FP and SP are the same
        l.mfspr r3,r0,17        # Get SR value
        l.ori   r3,r3,0x10      # Set exception enable bit
        l.jal   _main           # Jump to main routine
        l.mtspr r0,r3,17        # Enable exceptions (DELAY SLOT)

        .org    0xFFC
        l.nop                   # Guarantee the exception vector space
                                # does not have general purpose code
@end example

This is compiled and linked with the OpenRISC 1000 @sc{gnu} toolchain. Note
that the linking must specify the bootloader first and use the @code{-Ttext
0x0} argument.

@cindex OpenRISC 1000 Architectural Simulator, configuration
@cindex configuration, OpenRISC 1000 Architectural Simulator
@cindex Or1ksim, configuration
@cindex configuration, Or1ksim
The Or1ksim architectural simulator is configured with memory starting at
location 0x0. The debugging interface is enabled by using a debug section.

@example
section debug
  enabled         =          1
  rsp_enabled     =          1
  server_port     =      50000
end
@end example

The architectural simulator is started in its own terminal window. If the
configuration is in @code{rsp.cfg}, then the command might be:

@example
@command{or32-elf-sim -f rsp.cfg}
Reading script file from 'rsp.cfg'...
Building automata... done, num uncovered: 0/213.
Parsing operands data... done.
Resetting memory controller.
Resetting PIC.
@end example

Note that no program is specified - that will be loaded from @value{GDBN}.

In a separate window start up @value{GDBN}.

@example
@command{or32-elf-gdb}
@end example

A local copy of the symbol table is needed, specified with the @command{file}
command.

@cindex examples, symbol file loading
@cindex symbol file loading
@cindex symbols when remote debugging
@example
Building automata... done, num uncovered: 0/216.
Parsing operands data... done.
GNU gdb 6.8
Copyright (C) 2008 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=i686-pc-linux-gnu --target=or32-elf".
(gdb) @command{file hello}
Reading symbols from /home/jeremy/svntrunk/GNU/gdb-6.8/progs_or32/hello...done.
(gdb) 
@end example

@cindex examples, remote @command{target remote} command
@cindex remote @command{target remote} command, example
@cindex @command{target remote} command, remote, example
@cindex commands, @command{target remote}, remote, example
@cindex examples, remote target specification via RSP
@cindex remote target specification via RSP, example
@cindex target specification, remote via RSP, example
The connection to the target (the architectural simulator) is then
established, using the port number given in the configuration file.

@example
(gdb) @command{target remote :51000}
Remote debugging using :51000
0x00000100 in _start ()
(gdb) 
@end example

@cindex examples, program loading
@cindex program loading
@cindex program loading, example
@cindex program loading, remote
@cindex remote program loading, example
The program of interest can now be loaded:

@example
(gdb) @command{load hello}
Loading section .text, size 0x1290 lma 0x0
Loading section .rodata, size 0xe lma 0x1290
Start address 0x100, load size 4766
Transfer rate: 5 KB/sec, 238 bytes/write.
(gdb) 
@end example

The program does not immediately start running, since on opening the
connection to the target, Or1ksim stalls.

@cindex examples, @command{bt} command
@cindex @command{bt} command example
@cindex commands, @command{bt}, example
@cindex examples, @command{info spr} command
@cindex @command{info spr} command example
@cindex commands, @command{info spr}, example
All the GDB commands (including the SPR commands are available). For example

@example
(gdb) @command{bt}
#0  0x00000100 in _start ()
(gdb) @command{info spr 0 17}
SYS.SR = SPR0_17 = 32769 (0x8001)
(gdb) 
@end example

The Supervision Register shows the target is in Supervisor Mode and that SPRs
have User Mode read access.

@emph{Note.} The supervision register is used to provide the value for the
@value{GDBN} @code{$ps} processor status variable, so can also be accessed as:

@example
(gdb) @command{print $ps}
$1 = 32769
(gdb)
@end example

@cindex examples, @command{breakpoint} command
@cindex @command{breakpoint} command example
@cindex commands, @command{breakpoint}, example
@cindex examples, @command{continue} command
@cindex @command{continue} command example
@cindex commands, @command{continue}, example
@cindex continuening the remote program
@cindex examples, continuing a program
For this example set a breakpoint at the start of main and then continue the
program

@example
(gdb) @command{break main}
Breakpoint 1 at 0x1264: file hello.c, line 41.
(gdb) @command{continue}
Continuing.

Breakpoint 1, main () at hello.c:41
41        simputs( "Hello World!\n" );
(gdb) 
@end example

@cindex examples, @command{step} command
@cindex @command{step} command example
@cindex commands, @command{step}, example
It is now possible to step through the code:
@example
(gdb) @command{step}
simputs (str=0x1290 "Hello World!\n") at utils.c:90
90        for( i = 0; str[i] != '\0' ; i++ ) @{
(gdb) @command{step}
91          simputc( (int)(str[i]) );
(gdb) @command{step}
simputc (c=72) at utils.c:58
58        __asm__ __volatile__ ( "\tl.nop\t%0" : : "K"( NOP_PUTC ));
(gdb)
@end example

@cindex examples, @command{bt} command
@cindex @command{bt} command example
@cindex commands, @command{bt}, example
At this point a backtrace will show where the code has reached:

@example
(gdb) @command{bt}
#0  simputc (c=72) at utils.c:58
#1  0x000011cc in simputs (str=0x1290 "Hello World!\n") at utils.c:91
#2  0x00001274 in main () at hello.c:41
#3  0x00000118 in _start ()
(gdb) 
@end example

One more step completes the call to the character output routine. Inspecting
the terminal running the Or1ksim simulation, shows the output appearing:

@example
JTAG Proxy server started on port 50000
Resetting PIC.
H
@end example

@cindex examples, @command{continue} command
@cindex @command{continue} command example
@cindex commands, @command{continue}, example
Let the program run to completion by giving @value{GDBN} the continue command:
@example
(gdb) @command{continue}
Continuing.
Remote connection closed
(gdb) 
@end example

@cindex remote program termination
With completion of the program, the terminal running Or1ksim shows its final
output:

@example
Resetting PIC.
Hello World!
exit(0)
@@reset : cycles 0, insn #0
@@exit  : cycles 215892308, insn #215891696
 diff  : cycles 215892308, insn #215891696
@end example


@cindex remote program restart
@cindex restart, remote program
@cindex examples, @command{set} command
@cindex @command{set} command example
@cindex commands, @command{set}, example
When execution exits (by execution of a @code{l.nop 1}), the connection to the
target is automatically broken as the simulator exits.

@node OpenRISC 1000 Limitations
@chapter Known Problems
@cindex known problems
@cindex OpenRISC 1000, known GDB problems
@cindex bugs

There are some known problems with the current implementation

@enumerate
@item
@cindex known problems, watchpoints
@cindex bugs, watchpoints
If the OpenRISC 1000 Architecture supports hardware watchpoints, @value{GDBN}
will use them to implement hardware breakpoints and watchpoints. @value{GDBN}
is not perfect in handling of watchpoints. It is possible to allocate hardware
watchpoints and not discover until running that sufficient watchpoints are not
available. It is also possible that GDB will report watchpoints being hit
spuriously. This can be down to the assembly code having additional memory
accesses that are not obviously reflected in the source code.

@item
@cindex known problems, remote JTAG connection robustness
@cindex bugs, remote JTAG connection robustness
@cindex JTAG, remote connection robustness
@cindex remote JTAG, connection robustness
The remote JTAG connection is not robust to being interrupted, or
reconnecting. If the connection is lost due to error, then you must restart
GDB and the target server (for example the Or1ksim architectural
simulator). Moving to the Remote Serial Protocol is intended to remedy this
problem in the future.

@item
@cindex known problems, architectural compatability
@cindex bugs, architectural compatibility
@cindex GDB 5.3, differences in port of @value{GDBN} version @value{GDBVN}
The OpenRISC 1000 architecture has evolved since the port of GDB 5.3
in 2001. In particular the structure of the Unit Present register has
changed and the CPU Configuration register has been added. The port of
@value{GDBN} version @value{GDBVN} uses the @emph{current}
specification of the OpenRISC 1000. This means that old clients that
talk to the debugger may not work. In particular the Or1ksim
Architectural simulator requires a patch to work.

@item
@cindex known problems, Or1ksim architectural simulator
@cindex bugs, Or1ksim architectural simulator
@cindex Or1ksim, bugs fixed
The handling of watchpoints in the Or1ksim architectural simulator was
incorrect. To work with @value{GDBN} @value{GDBVN}, a patch is required to fix
this problem. This is combined with the patch changing the structure of the
Unit Present and CPU Configuration registers.

@item
@cindex known problems, Or1ksim architectural simulator
@cindex bugs, Or1ksim architectural simulator
@cindex Or1ksim, bugs fixed
The OpenRISC 1000 architecture uses its General Purpose
Register (GPR) 2 as a frame pointer register. However the @command{$fp}
variable in @value{GDBN} is not currently implemented, and will return
the value of the stack pointer (GPR 1) instead.
@end enumerate

@cindex Bugs, reporting
@cindex Reporting bugs
Reports of bugs are much welcomed. Please report problems through the
OpenCORES tracker at @uref{www.opencores.org/ptracker.cgi/list/or1k}.
@include gpl.texi

@raisesections
@include fdl.texi
@lowersections

@node Index
@unnumbered Index

@printindex cp

@tex
% I think something like @colophon should be in texinfo.  In the
% meantime:
\long\def\colophon{\hbox to0pt{}\vfill
\centerline{The body of this manual is set in}
\centerline{\fontname\tenrm,}
\centerline{with headings in {\bf\fontname\tenbf}}
\centerline{and examples in {\tt\fontname\tentt}.}
\centerline{{\it\fontname\tenit\/},}
\centerline{{\bf\fontname\tenbf}, and}
\centerline{{\sl\fontname\tensl\/}}
\centerline{are used for emphasis.}\vfill}
\page\colophon
% Blame: doc@cygnus.com, 1991.
@end tex

@bye
