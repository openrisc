2008-05-05  Alan Modra  <amodra@bigpond.net.au>

	PR 6473
	* ld-elf/lma.s, ld-elf/lma.lnk: New test.
	* ld-elf/binutils.exp: Run it.

2008-05-03  Mike Frysinger  <vapier@gentoo.org>

	* ld-scripts/defined.s: Use .set syntax rather than =.

2008-05-02  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/6475
	* ld-scripts/rgn-over8-ok.d: Accept any alignment.

2008-04-29  Daniel Jacobowitz  <dan@codesourcery.com>

	* ld-arm/symbian-seg1.s, ld-arm/symbian-seg1.d: New files.
	* ld-arm/arm-elf.exp: Run symbian-seg1.

2008-04-28  Nathan Sidwell  <nathan@codesourcery.com>

	* ld-scripts/rgn-over8.s: Tweak.

	* ld-scripts/rgn-over.exp: Allow -ok file names to pass.
	* ld-scripts/rgn-over8.s: New.
	* ld-scripts/rgn-over8.t: New.
	* ld-scripts/rgn-over8-ok.d: New.

2008-04-21  Nathan Sidwell  <nathan@codesourcery.com>

	* ld-vxworks/plt-mips1.s: New.
	* ld-vxworks/plt-mips1.d: New.

2008-04-16  David S. Miller  <davem@davemloft.net>

	* ld-sparc/gotop32.dd: New.
	* ld-sparc/gotop32.rd: Likewise.
	* ld-sparc/gotop32.s: Likewise.
	* ld-sparc/gotop32.sd: Likewise.
	* ld-sparc/gotop32.td: Likewise.
	* ld-sparc/gotop64.dd: Likewise.
	* ld-sparc/gotop64.rd: Likewise.
	* ld-sparc/gotop64.s: Likewise.
	* ld-sparc/gotop64.sd: Likewise.
	* ld-sparc/gotop64.td: Likewise.
	* ld-sparc/sparc.exp: Run new gotdata tests.

2008-04-15  Andrew Stubbs  <andrew.stubbs@st.com>

	* ld-sh/arch/sh-dsp.s: Regenerate.
	* ld-sh/arch/sh.s: Regenerate.
	* ld-sh/arch/sh2.s: Regenerate.
	* ld-sh/arch/sh2a-nofpu-or-sh3-nommu.s: Regenerate.
	* ld-sh/arch/sh2a-nofpu-or-sh4-nommu-nofpu.s: Regenerate.
	* ld-sh/arch/sh2a-nofpu.s: Regenerate.
	* ld-sh/arch/sh2a-or-sh3e.s: Regenerate.: Regenerate.
	* ld-sh/arch/sh2a-or-sh4.s: Regenerate.
	* ld-sh/arch/sh2a.s: Regenerate.
	* ld-sh/arch/sh2e.s: Regenerate.
	* ld-sh/arch/sh3-dsp.s: Regenerate.
	* ld-sh/arch/sh3-nommu.s: Regenerate.
	* ld-sh/arch/sh3.s: Regenerate.
	* ld-sh/arch/sh3e.s: Regenerate.
	* ld-sh/arch/sh4-nofpu.s: Regenerate.
	* ld-sh/arch/sh4-nommu-nofpu.s: Regenerate.
	* ld-sh/arch/sh4.s: Regenerate.
	* ld-sh/arch/sh4a-nofpu.s: Regenerate.
	* ld-sh/arch/sh4a.s: Regenerate.
	* ld-sh/arch/sh4al-dsp.s: Regenerate.

2008-04-08  Alan Modra  <amodra@bigpond.net.au>

	* ld-spu/ovl2.s: Extend to test jump table references and
	absolute _SPUEAR_ syms.
	* ld-spu/ovl2.d: Update.

2008-03-28  Joseph Myers  <joseph@codesourcery.com>

	* ld-elfcomm/elfcomm.exp: Run $READELF not readelf.

2008-03-26  Daniel Jacobowitz  <dan@codesourcery.com>

	* ld-elf/flags1.d: Adjust for MIPS text alignment.

2008-03-25  Nathan Sidwell  <nathan@codesourcery.com>

	* ld-vxworks/tls-3.s: New.
	* ld-vxworks/tls-3.d: New.

2008-03-22  Hans-Peter Nilsson  <hp@axis.com>

	* ld-cris/libdso-10.d: Adjust for change in objdump output.

2008-03-21  Adam Nemet  <anemet@caviumnetworks.com>

	* ld-mips-elf/dyn-sec64.d, ld-mips-elf/dyn-sec64.s,
	ld-mips-elf/dyn-sec64.ld: New test.
	* ld-mips-elf/mips-elf.exp: Run it.

2008-03-20  Richard Sandiford  <rsandifo@nildram.co.uk>

	* ld-mips-elf/got-dump-1.d, ld-mips-elf/got-dump-1.s,
	ld-mips-elf/got-dump-1.ld, ld-mips-elf/got-dump-2.d,
	ld-mips-elf/got-dump-2.s, ld-mips-elf/got-dump-2.ld: New tests.
	* ld-mips-elf/mips-elf.exp: Run them.

2008-03-20  Richard Sandiford  <rsandifo@nildram.co.uk>

	* ld-mips-elf/elf-rel-got-n64-linux.d: Expect bit 63 rather than
	bit 31 of the second GOT entry to be set.
	* ld-mips-elf/elf-rel-got-n64.d: Likewise.
	* ld-mips-elf/elf-rel-xgot-n64-linux.d: Likewise.
	* ld-mips-elf/elf-rel-xgot-n64.d: Likewise.

2008-03-17  Richard Sandiford  <rsandifo@nildram.co.uk>

	* ld-mips-elf/eh-frame1-n32.d: Expect a warning about .eh_frame_hdr.
	Remove duplicate CIEs.  Adjust relocation addresses and .eh_frame
	offsets accordingly.  Do not allow there to be any trailing
	R_MIPS_NONE relocations.
	* ld-mips-elf/eh-frame1-n64.d: Likewise.
	* ld-mips-elf/eh-frame2-n32.d: Likewise.
	* ld-mips-elf/eh-frame2-n64.d: Likewise.

2008-03-17  Richard Sandiford  <rsandifo@nildram.co.uk>

	* ld-mips-elf/mips-elf.exp (o32_as_flags, o32_ld_flags): New variables.
	(mips16_call_global_test, mips16_intermix_test): Use them.

2008-03-16  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/5789
	PR ld/5943
	* ld-i386/hidden1.d: New.
	* ld-i386/hidden1.s: Likewise.
	* ld-i386/hidden2.d: Likewise.
	* ld-i386/hidden2.s: Likewise.
	* ld-i386/hidden3.d: Likewise.
	* ld-i386/hidden4.s: Likewise.
	* ld-i386/protected1.d: Likewise.
	* ld-i386/protected1.s: Likewise.
	* ld-i386/protected2.d: Likewise.
	* ld-i386/protected2.s: Likewise.
	* ld-i386/protected3.d: Likewise.
	* ld-i386/protected3.s: Likewise.
	* ld-x86-64/hidden1.d: Likewise.
	* ld-x86-64/hidden1.s: Likewise.
	* ld-x86-64/hidden2.d: Likewise.
	* ld-x86-64/hidden2.s: Likewise.
	* ld-x86-64/hidden3.d: Likewise.
	* ld-x86-64/hidden3.s: Likewise.
	* ld-x86-64/protected1.d: Likewise.
	* ld-x86-64/protected1.s: Likewise.
	* ld-x86-64/protected2.d: Likewise.
	* ld-x86-64/protected2.s: Likewise.
	* ld-x86-64/protected3.d: Likewise.
	* ld-x86-64/protected3.s: Likewise.

	* ld-i386/i386.exp: Run hidden1, hidden2, hidden3, protected1,
	protected2 and protected3.
	* ld-x86-64/x86-64.exp: Likewise.

2008-03-14  Alan Modra  <amodra@bigpond.net.au>

	* ld-spu/ovl2.s: Make setjmp global.
	* ld-spu/ovl2.d: Update.

2008-03-12  Alan Modra  <amodra@bigpond.net.au>

	PR 5900
	* ld-elf/sec64k.exp: Update.

2008-03-08  Paul Brook  <paul@codesourcery.com>

	* ld-arm/arm-elf.exp (armeabitests): Add thumb2-b-interwork.
	* ld-arm/thumb2-b-interwork.d: New test.
	* ld-arm/thumb2-b-interwork.s: New test.

2008-03-07  Paul Brook  <paul@codesourcery.com>

	* ld-arm/arm-elf.exp (armelftests): Add movw-merge and arm-app-movw.
	* ld-arm/arm-app-movw.s: New test.
	* ld-arm/arm-app.r: Update expected output.
	* ld-arm/movw-merge.d: New test.
	* ld-arm/movw-merge.s: New test.

2008-03-01  Alan Modra  <amodra@bigpond.net.au>

	* ld-powerpc/relbrlt.d: Update.  Also check .branch_lt section.

2008-02-27  Catherine Moore  <clm@codesourcery.com>

	* ld-cris/libdso-10.d: Update expected output for the Dynamic
	Section to allow an arbitrary number of spaces.

2008-02-20  Mark Mitchell  <mark@codesourcery.com>

	ld/testsuite/
	* ld-elf/seg.d: Expect .reginfo section on MIPS.

2008-02-20  Pedro Alves  <pedro_alves@portugalmail.pt>

	* ld-auto-import/auto-import.exp: Use $ld to link the dll for
	cygwin, not $CC.

2008-02-18  Hans-Peter Nilsson  <hp@axis.com>

	* lib/ld-lib.exp (run_dump_test): Don't apply prune_warnings
	for tool invocations where warnings or errors can be matched.

2008-02-14  H.J. Lu  <hongjiu.lu@intel.com>

	* ld-shared/sh1.c (shlib_overriddencall2): Moved to ...
	* ld-shared/sh2.c (shlib_overriddencall2): Here.  New.

2008-02-07  Alan Modra  <amodra@bigpond.net.au>

	* ld-spu/ovl.d: Update.
	* ld-spu/ovl2.d: Update.

2008-02-04  Bob Wilson  <bob.wilson@acm.org>

	* ld-undefined/undefined.exp: XFAIL for xtensa*-*-linux*.
	
2008-01-31  Marc Gauthier  <marc@tensilica.com>

	* ld-elf/merge.d: Recognize Xtensa processor variants.
	* ld-xtensa/coalesce.exp: Likewise.
	* ld-xtensa/lcall.exp: Likewise.

2008-01-28  Petr Muller  <pmuller@redhat.com>

	* ld-elfvers/vers.exp (test_ar): Sort the expected output so that
	it has matches the ordering of the obtained output.

2008-01-28  H.J. Lu  <hongjiu.lu@intel.com>

	* ld-elf/eh1.d: Replace DW_CFA_def_cfa_reg with
	DW_CFA_def_cfa_register. Updated for i386/x86-64 register
	names.
	* ld-elf/eh2.d: Likewise.
	* ld-elf/eh3.d: Likewise.
	* ld-elf/eh4.d: Likewise.
	* ld-elf/eh5.d: Likewise.

2008-01-28  Alan Modra  <amodra@bigpond.net.au>

	* ld-spu/ovl.d: Update.
	* ld-spu/ovl2.d: Update.

2008-01-26  Alan Modra  <amodra@bigpond.net.au>

	* ld-elf/loadaddr.t: New, extracted from..
	* ld-elf/loadaddr1.t: ..here.  Use insert.
	* ld-elf/loadaddr2.t: Likewise.
	* ld-elf/loadaddr1.d: Update.
	* ld-elf/loadaddr2.d: Update.

2008-01-25  Alan Modra  <amodra@bigpond.net.au>

	* ld-spu/ovl.lnk: Delete overlay.
	* ld-spu/ovl1.lnk: New file.
	* ld-spu/ovl2.lnk: New file.
	* ld-spu/ovl.d: Update.
	* ld-spu/ovl2.d: Update.

2008-01-23  Andreas Schwab  <schwab@suse.de>

	* ld-gc/gc.c: Make sure used_func is not inlined.

2008-01-22  H.J. Lu  <hongjiu.lu@intel.com>

	* ld-gc/gc.exp: Use [which $CC] != 0.

2008-01-14  Tristan Gingold  <gingold@adacore.com>

	* ld-gc/gc.exp (test_gc): Let missing C compiler make tests
	"untested" instead of "failed".

2008-01-10  Tristan Gingold  <gingold@adacore.com>

	* lib/ld-lib.exp (check_gc_sections_available): Now available on
	VxWorks.
	* ld-gc: New directory for testing --gc-sections.
	* ld-gc/gc.c: New file.
	* ld-gc/gc.exp: New file.
	* ld-gc/noent.s: New file.
	* ld-gc/noent.d: New file.

2008-01-09  Richard Sandiford  <rsandifo@nildram.co.uk>

	PR ld/5526
	* ld-elf/eh6.s, ld-elf/eh6.d: New test.

2008-01-07  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/5522
	* ld-elf/noload-3.d: New file.
	* ld-elf/noload-3.s: Likewise.
	* ld-elf/noload-3.t: Likewise.

For older changes see ChangeLog-2007

Local Variables:
mode: change-log
left-margin: 8
fill-column: 74
version-control: never
End:
